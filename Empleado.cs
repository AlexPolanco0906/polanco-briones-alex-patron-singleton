﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patrón_Singleton
{
    //Realizamos una herencia de datos en esta clase Empleado 
    public class Empleado : Computadora
    {
        //Creada esta clase procedemos a crear campos para la clase 
        protected string nombreEmpleado;
        protected int cedula;

        //Los validamos con su respectivo tipo de dato 
        public string NombreEmpleado
        {
            set
            {
                nombreEmpleado = value;
            }
            get
            {
                //Retornamos datos 
                return nombreEmpleado;
            }
        }

        //Los validamos con su respectivo tipo de dato 
        public int Cedula 
        {
            set
            {
                cedula = value;
            }
            get
            {
                //Retornamos datos 
                return cedula;
            }
        }
        
        //Le datos el nombre segun la acción que deseamos que realize 
        new public void Imprimir()
        {
            base.Imprimir();
            //Imprimimos las propiedades de la clase con su respectivo mensaje, como atractivo visual.
            Console.WriteLine("El nombre del empleado es...." + nombreEmpleado);
            Console.WriteLine("El número de cecula del empleado es....." + cedula);
        }
    }
}