﻿using System;
//Nombre: Alex Javier Polanco Briones 
//Curso: 3"A"
//Materia: Programación Orientada a Objetos 
//Tema: Implementación del patron de diseño singleton en un ejemplo práctico 
namespace Patrón_Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            //No se puede obtener la instancia directamente 
            //Obtenemos la instancia, se crea por primera vez 
            Impresora uno = Impresora.Obteninstancia();

            //Hacemos algo con la instancia 
            uno.PonerDatos("Alex Polanco", 1314195763);
            uno.AlgunProceso();
            Console.WriteLine(uno);
            Console.WriteLine("-----");

            //Obtenemos la instancia 
            Impresora dos = Impresora.Obteninstancia();
            //Comprobamos que es la misma instancia 
            //Si lo es, tendra el mismo estado 
            Console.WriteLine(dos);
            //Console.WriteLine("Hello World!");

            //Llamamos a nuestro metodo, la forma es la tradicional 
            //A continuación hacemos uso de los componentes propios de esas clases y llenamos con datos
            Computadora persona1 = new Computadora();
            persona1.NombrePC = "PC1001CYBERJUAN";
            persona1.CodigoPC = 1001;
            Console.WriteLine("Los datos de la computadora son....: ");
            //Imprimimos los datos llenados de este método 

            persona1.Imprimir();


            //Llamamos a nuestro metodo, la forma es la tradicional 
            //A continuación hacemos uso de los componentes propios de esas clases y llenamos con datos
            Empleado empleado1 = new Empleado();
            empleado1.NombrePC= "PC1002CYBERJUAN";
            empleado1.CodigoPC = 1002;
            empleado1.NombreEmpleado = "Alex Polanco";
            empleado1.Cedula = 1314195763; 
            Console.WriteLine("Los datos del empleado 1 son...: ");
            //Imprimimos los datos llenados de este método 

            empleado1.Imprimir();

            //Llamamos a nuestro metodo, la forma es la tradicional 
            //A continuación hacemos uso de los componentes propios de esas clases y llenamos con datos
            Empleado empleado2 = new Empleado();
            empleado2.NombrePC = "PC1003CYBERJUAN";
            empleado2.CodigoPC = 1003;
            empleado2.NombreEmpleado = "Kely Sanchez";
            empleado2.Cedula = 1314187365;
            Console.WriteLine("Los datos del empleado 2 son...: ");
            //Imprimimos los datos llenados de este método 
            empleado2.Imprimir();


            Console.ReadKey();
        }
    }
}
