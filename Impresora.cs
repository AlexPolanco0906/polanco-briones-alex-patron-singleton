﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patrón_Singleton
{
    public class Impresora
    {
        //En esta clase esta nuestro singleton "Impresora".
            private static Impresora instancia;
            private string nombreempleado;
            private int cedula;

            //Creamos el constructor privado 
            private Impresora()
            {
            //Datos propios del constructor 
                nombreempleado = "Alex Polanco";
                cedula = 1314195763;
            }

        //A continuación realizamos uno de los procesos necesarios al momento que implementamos singleton 
            public static Impresora Obteninstancia()
            {
                //Verificamos si no  existe la instancia
                if (instancia == null)
                {
                    //Si no existe, instanciamosS
                    instancia = new Impresora();
                    Console.WriteLine("---Instancia creada por primera vez----");
                }

                //Regreamos la instanica 
                return instancia;
            }

            //Aqui ponemos metodos propios de la clase 
            //Que en este caso son datos para mostrar por pantalla 
            public override string ToString()
            {
            //Retornamos datos junto con su respectivo mensaje para la presentación de los mismos 
                return string.Format("La persona {0}, con número de cedula {1}", nombreempleado, cedula);
            }

        //Ponemos datos, guardamos en variables del mismo nombre solo con ligeros cambios de escritura
            public void PonerDatos(string pNombreempleado, int pCedula)
            {
                nombreempleado = pNombreempleado;
                cedula = pCedula;
            }
            //Esto representa cualquier otro objeto, que para este ejemplo solo son impresiones por pantalla 
            public void AlgunProceso()
            {
            //Imprimimos por pantalla el siguiente mensaje 
                Console.WriteLine("{0} esta trabajando en algo", nombreempleado);

            }
        
    }
}