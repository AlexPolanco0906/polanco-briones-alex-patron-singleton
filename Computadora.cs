﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patrón_Singleton
{
    public class Computadora
    {
        //Creada esta clase procedemos a crear campos para la clase 
        protected string nombrePC;
        protected int codigoPC; 

        //Los validamos con su respectivo tipo de dato 
        public string NombrePC
        {
            set
            {
                nombrePC = value;
            }
            get
            {
                //Retornamos datos 
                return nombrePC;
            }
        }

        //Los validamos con su respectivo tipo de dato 
        public int CodigoPC
        {
            set
            {
                codigoPC= value;
            }
            get
            {
                //Retornamos datos 
                return codigoPC;
            }
        }

        //Le datos el nombre segun la acción que deseamos que realize 
        public void Imprimir()
        {
            //Imprimimos las propiedades de la clase con su respectivo mensaje, como atractivo visual.
            Console.WriteLine("Nombre de la PC: " + NombrePC);
            Console.WriteLine("Código de la PC: " + CodigoPC);
          
        }
    }
}